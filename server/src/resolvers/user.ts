import * as jwt from 'jsonwebtoken'
import { IResolvers } from 'apollo-server-express'
import * as Models from '../models'

const { users: User } = Models

const { JWT_SECRET } = process.env

const generateToken = ({ id, email, username }) =>
  jwt.sign({ id, email, username }, JWT_SECRET)

const userResolver: IResolvers = {
  Query: {
    getUsers: (): any => {
      return [{ id: 1 }]
    }
  },
  Mutation: {
    createUser: async (_, { email, username }): Promise<any> => {
      try {
        const newUser = await User.findOrCreate({
          where: { email },
          defaults: { email, username }
        })

        const userObj = newUser[0] ? newUser[0].dataValues : newUser.dataValues
        return {
          user: newUser[0].dataValues,
          token: generateToken({ ...userObj })
        }
      } catch (error) {
        throw new Error(error.message)
      }
    }
  }
}

export default userResolver
