import * as _ from 'lodash'
import { IResolvers } from 'apollo-server-express'
import * as crypto from 'crypto'
import * as zlib from 'zlib'
import * as Models from '../models'
import { redisSet, redisGet } from '../config/redis'

const { app: App, users: User, Sequelize } = Models

const appResolver: IResolvers = {
  Query: {
    getUserApps: async (p, {}, { user }): Promise<any> => {
      try {
        if (!user) throw new Error('Session has expired. Log in again.')
        const apps = await App.findAll({
          where: Sequelize.or(
            { userId: Number(user.id) },
            { team: { [Sequelize.Op.contains]: [user.email] } }
          ),
          raw: true,
          order: [['createdAt', 'DESC']]
        })

        return { apps }
      } catch (error) {
        throw new Error(error.message)
      }
    },
    syncData: async (_, { payload }, {}): Promise<any> => {
      let payloadObj = { ...payload }
      try {
        const app = await App.findOne({ where: { apikey: payloadObj.apikey } })
        if (!app) throw new Error('App does not exist')
        if (app.models.includes(payload.model)) {
          await redisSet(
            `${app.name}-${payloadObj.model}`,
            JSON.stringify({ ...payloadObj.data })
          )
        }
        return 'OK'
      } catch (error) {
        throw new Error(error.message)
      }
    },
    getDataForModel: async (_, { id, model }, {}): Promise<any> => {
      try {
        const app = await App.findByPk(id)
        if (!app) throw new Error('App does not exist')

        const modelExists = app.models.includes(model)

        if (!modelExists) throw new Error('Model does not exist')

        const data = await redisGet(`${app.id}-${app.name}-${model}`)

        if (!data) return { data: [] }
        const unzipped = zlib.inflateSync(Buffer.from(data, 'base64'))
        const result = JSON.parse(unzipped.toString())

        return { data: result }
      } catch (error) {
        throw new Error(error.message)
      }
    },
    isAllWell: async (_, { apikey }, {}): Promise<any> => {
      try {
        const app = await App.findOne({ where: { apikey } })
        if (!app) throw new Error('Invalid API Key')
        return 'OK'
      } catch (error) {
        throw new Error(error.message)
      }
    },
    getAppDetails: async (_, { id }, { user }): Promise<any> => {
      try {
        const app = await App.findByPk(id)
        const team = app.team || []
        if (!app || (app.userId !== user.id && !team.includes(user.email)))
          throw new Error('App does not exist')

        const models = app.models

        const data = {
          ...app.dataValues
        }

        for (let index = 0; index < (models?.length || [].length); index++) {
          const model = models[index]
          data[model] =
            Number(await redisGet(`${app.id}-${app.name}-${model}-stat`)) || 0
        }

        return { data }
      } catch (error) {
        throw new Error(error.message)
      }
    }
  },
  Mutation: {
    createApp: async (_, { name }, { user }): Promise<any> => {
      try {
        if (!user) throw new Error('Session has expired. Log in again.')
        const app = await App.create({
          name,
          userId: user.id,
          apikey: String(crypto.randomBytes(20).toString('hex'))
        })

        return { app }
      } catch (error) {
        throw new Error(error.message)
      }
    },
    updateApp: async (p, payload, { user }): Promise<any> => {
      try {
        if (!user) throw new Error('Session has expired. Log in again.')
        const app = await App.findByPk(payload.id)

        if (!app) throw new Error('App does not exist')

        let editables: any = { ...payload }

        if (payload.hasOwnProperty('models')) {
          editables.models = [...payload.models]
          if (app.models !== null)
            editables.models = _.uniq([...editables.models, ...app.models])
        }

        await app.update(editables)

        return { app }
      } catch (error) {
        throw new Error(error.message)
      }
    },
    deleteModel: async (p, payload, { user }): Promise<any> => {
      try {
        if (!user) throw new Error('Session has expired. Log in again.')
        const app = await App.findByPk(payload.id)

        if (!app) throw new Error('App does not exist')

        const models: any[] = app.models

        const modelToDelete = payload.model
        models.splice(models.indexOf(modelToDelete), 1)

        await app.update({ models })

        return { app }
      } catch (error) {
        throw new Error(error.message)
      }
    },
    regenerateKey: async (_, { id }, { user }): Promise<any> => {
      try {
        if (!user) throw new Error('Session has expired. Log in again.')

        const app = await App.findByPk(id)
        if (!app) throw new Error('App does not exist')

        const newkey = crypto.randomBytes(20).toString('hex')

        await app.update({ apikey: newkey })

        return { app }
      } catch (error) {
        throw new Error(error.message)
      }
    },
    addTeammember: async (_, { id, email }, { user }): Promise<any> => {
      try {
        if (!user) throw new Error('Session has expired. Log in again.')
        const app = await App.findByPk(id)
        const userExists = await User.findOne({ where: { email } })
        if (!userExists) throw new Error('User does not exist')
        if (!app) throw new Error('App does not exist')
        const currentTeam = app.team || []
        if (currentTeam.includes(email))
          throw new Error('This user is already in your team.')
        const team = [...currentTeam, email]
        await app.update({ team })
        return { app }
      } catch (error) {
        throw new Error(error.message)
      }
    },
    removeTeammember: async (_, { id, email }, { user }): Promise<any> => {
      try {
        if (!user) throw new Error('Session has expired. Log in again.')
        const app = await App.findByPk(id)
        const userExists = User.findOne({ where: { email } })
        if (!userExists) throw new Error('User does not exist')
        if (!app) throw new Error('App does not exist')
        if (!app.team.includes(email))
          throw new Error('This user is not in your team.')
        const team: any[] = app.team
        team.splice(team.indexOf(email), 1)
        await app.update({ team })
        return { app }
      } catch (error) {
        throw new Error(error.message)
      }
    }
  }
}

export default appResolver
