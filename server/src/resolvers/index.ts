import { IResolvers } from 'apollo-server-express'
import userResolver from './user'
import appResolver from './app'

const resolvers: IResolvers = {
  userResolver,
  appResolver
}

export default resolvers
