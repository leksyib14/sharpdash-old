import { gql } from 'apollo-server-express'

const schema = gql`
  scalar JSON

  type User {
    id: String!
    username: String
    email: String
  }
  type App {
    id: String!
    apikey: String!
    name: String!
    models: [String]
    plan: String
    subscription: String
    team: [String]
  }
  type UserResponse {
    user: User!
  }
  type AuthResponse {
    user: User!
    token: String!
  }
  type AppResponse {
    app: App
  }
  type AppResponse2 {
    apps: [App]
  }
  type JSONResponse {
    data: JSON
  }
  type Query {
    getUsers: UserResponse!
    getUserApps: AppResponse2!
    syncData(payload: JSON): String!
    getDataForModel(id: Int, model: String): JSONResponse
    isAllWell(apikey: String!): String!
    getAppDetails(id: Int!): JSONResponse
  }
  type Mutation {
    createUser(email: String, username: String): AuthResponse!
    createApp(name: String!): AppResponse!
    regenerateKey(id: Int!): AppResponse!
    updateApp(
      id: Int!
      models: [String]
      name: String
      plan: String
      subscriptionId: String
    ): AppResponse!
    deleteModel(id: Int!, model: String): AppResponse!
    addTeammember(id: Int!, email: String): AppResponse!
    removeTeammember(id: Int!, email: String): AppResponse!
  }
`

export default schema
