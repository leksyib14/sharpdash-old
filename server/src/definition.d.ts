interface UserType {
  id: number
  name: string
  email: string
  createdAt: string
  updatedAt: string
}
