import { redisSet } from '../config/redis'
import * as Models from '../models'
import * as zlib from 'zlib'

const { app: App } = Models

export const syncData = async (req, res) => {
  let payloadObj = { ...req.body }

  try {
    const app = await App.findOne({ where: { apikey: payloadObj.apikey } })
    if (!app) return res.status(400).send('App does not exist')
    if (app.models.includes(payloadObj.modelName)) {
      let data = JSON.parse(
        zlib.inflateSync(Buffer.from(payloadObj.data.data)).toString()
      )

      if (app.plan === 'free') {
        data = [...data].slice(0, 2000)
      } else if (app.plan === 'startup') {
        data = [...data].slice(0, 40000)
      }

      zlib.deflate(JSON.stringify(data), (err, zipped) => {
        if (err) {
          console.log('Error deflating!')
          return
        }
        redisSet(
          `${app.id}-${app.name}-${payloadObj.modelName}`,
          zipped.toString('base64')
        )
      })
      await redisSet(
        `${app.id}-${app.name}-${payloadObj.modelName}-stat`,
        String(data.length)
      )
    } else {
      return res
        .status(400)
        .send(
          `${payloadObj.modelName} model does not exist on this app. Make sure to create the model before adding it here.`
        )
    }
    return res.status(200).send('OK')
  } catch (error) {
    console.log(error)
    return res.status(400).send(error.message)
  }
}
