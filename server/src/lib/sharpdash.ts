require('dotenv').config()
import { SharpDashBoard } from 'sharpdashboard-sdk-node'
import * as Models from '../models'

const { users: User, app: App } = Models

const getUsers = async () => {
  try {
    const users = await User.findAll({ raw: true })
    return users
  } catch (error) {
    console.log(error.message)
  }
}

const getApps = async () => {
  try {
    const apps = await App.findAll({
      raw: true,
      attributes: ['id', 'name', 'createdAt', 'updatedAt', 'plan']
    })
    return apps
  } catch (error) {
    console.log(error.message)
  }
}

const dashboard = new SharpDashBoard(process.env.SHARPDASHBOARD_API_KEY)

dashboard.addModelHandler('users', getUsers)
dashboard.addModelHandler('apps', getApps)

export default dashboard
