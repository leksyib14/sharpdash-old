import * as express from 'express'
import { syncData } from './rest'

const router = express.Router()

router.post('/sync', syncData)

export default router
