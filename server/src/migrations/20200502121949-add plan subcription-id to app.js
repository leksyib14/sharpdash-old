'use strict'

module.exports = {
  up: (queryInterface, Sequelize) =>
    queryInterface.addColumn('apps', 'subscriptionId', {
      type: Sequelize.STRING,
      allowNull: true
    }),

  down(queryInterface) {
    return queryInterface.removeColumn('apps', 'subscriptionId')
  }
}
