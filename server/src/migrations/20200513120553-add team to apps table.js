'use strict'

module.exports = {
  up: (queryInterface, Sequelize) =>
    queryInterface.addColumn('apps', 'team', {
      type: Sequelize.ARRAY(Sequelize.STRING),
      allowNull: true
    }),

  down(queryInterface) {
    return queryInterface.removeColumn('apps', 'team')
  }
}
