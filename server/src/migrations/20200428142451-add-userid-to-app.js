'use strict'

module.exports = {
  up: (queryInterface, Sequelize) =>
    queryInterface.addColumn('apps', 'userId', {
      type: Sequelize.INTEGER,
      allowNull: true
    }),

  down(queryInterface) {
    return queryInterface.removeColumn('apps', 'userId')
  }
}
