'use strict'

module.exports = {
  up: (queryInterface, Sequelize) =>
    queryInterface.addColumn('apps', 'plan', {
      type: Sequelize.STRING,
      allowNull: true
    }),

  down(queryInterface) {
    return queryInterface.removeColumn('apps', 'plan')
  }
}
