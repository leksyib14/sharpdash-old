const { config } = require('dotenv')

config()

exports.development = {
  url: process.env.DATABASE_URL,
  dialect: 'postgres',
  logging: false
}
exports.test = {
  url: process.env.DATABASE_URL,
  dialect: 'postgres',
  logging: false
}
exports.production = {
  url: process.env.DATABASE_URL,
  dialect: 'postgres',
  logging: false
}
