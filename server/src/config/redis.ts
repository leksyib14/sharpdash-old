require('dotenv').config()
import * as redis from 'redis'
import * as util from 'util'

const client = redis.createClient({
  url:
    process.env.NODE_ENV === 'development'
      ? process.env.REDIS_URL
      : 'redis://127.0.0.1:6379',
  detect_buffers: true
})

let redisSet = util.promisify(client.set).bind(client)
let redisGet = util.promisify(client.get).bind(client)

export { redisSet, redisGet }
