'use strict'
module.exports = (sequelize, DataTypes) => {
  const user = sequelize.define(
    'users',
    {
      username: DataTypes.STRING,
      email: DataTypes.STRING
    },
    {}
  )
  user.associate = function(models) {
    // associations can be defined here
    user.hasMany(models.app, {
      foreignKey: 'id',
      onDelete: 'CASCADE',
      as: 'apps'
    })
  }
  return user
}
