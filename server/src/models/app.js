'use strict'
module.exports = (sequelize, DataTypes) => {
  const app = sequelize.define(
    'app',
    {
      name: DataTypes.STRING,
      apikey: DataTypes.STRING,
      userId: DataTypes.INTEGER,
      models: DataTypes.ARRAY(DataTypes.STRING),
      plan: {
        type: DataTypes.STRING,
        defaultValue: 'free'
      },
      team: {
        type: DataTypes.ARRAY(DataTypes.STRING)
      },
      subscriptionId: DataTypes.STRING
    },
    {}
  )
  app.associate = function(models) {
    // associations can be defined here
    app.belongsTo(models.users, {
      foreignKey: 'userId',
      onDelete: 'CASCADE'
    })
  }
  return app
}
