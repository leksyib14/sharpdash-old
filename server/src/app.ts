import { merge } from 'lodash'
import * as path from 'path'
import * as express from 'express'
import * as bodyParser from 'body-parser'
import * as fileUpload from 'express-fileupload'
import { ApolloServer } from 'apollo-server-express'
import * as dotenv from 'dotenv'
import typeDefs from './schema'
import * as jwt from 'jsonwebtoken'
import resolvers from './resolvers'
import router from './lib/routes'
require('dotenv').config
import './lib/sharpdash'

const { userResolver, appResolver } = resolvers

const app = express()

app.use(fileUpload())

const server = new ApolloServer({
  typeDefs,
  resolvers: merge(userResolver, appResolver),
  context: ({ req }) => {
    const tokenWithBearer: string = String(req.headers.auth) || ''
    return { user: getUser(tokenWithBearer.replace('Bearer ', '')) }
  },
  cacheControl: false,
  introspection: process.env.NODE_ENV === 'development',
  playground: process.env.NODE_ENV === 'development'
})

server.applyMiddleware({ app })

app.use(bodyParser.urlencoded({ extended: true, limit: '50mb' }))
app.use(bodyParser.json({ limit: '50mb' }))

app.use(express.static(path.join(__dirname, '../../client/build')))

// Handle React routing, return all requests to React app
app.get('*', function(req, res) {
  res.sendFile(path.join(__dirname, '../../client/build', 'index.html'))
})
// app.get('/dashboard', function(req, res) {
//   res.sendFile(path.join(__dirname, '../../client/build', 'index.html'))
// })

app.set('Accept-Encoding', '*')

dotenv.config()
const { JWT_SECRET } = process.env

app.use('/app', router)

const getUser = token => {
  try {
    if (token) {
      return jwt.verify(token, JWT_SECRET)
    }
    return null
  } catch (err) {
    return null
  }
}

app.listen({ port: process.env.PORT, hostname: '0.0.0.0' }, () => {
  console.log(`Server listening at ${process.env.PORT} -->`)
})
