import { gql } from 'apollo-boost'

export const GET_USER_PROJECTS = gql`
  query {
    getUserApps {
      apps {
        id
        name
        apikey
        models
        plan
      }
    }
  }
`

export const GET_PROJECT_DETAILS = gql`
  query GetDetails($id: Int!) {
    getAppDetails(id: $id) {
      data
    }
  }
`

export const GET_MODEL_DATA = gql`
  query GetModelData($id: Int, $model: String) {
    getDataForModel(id: $id, model: $model) {
      data
    }
  }
`
