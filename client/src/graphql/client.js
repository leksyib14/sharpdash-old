import { ApolloClient, InMemoryCache, from } from 'apollo-client-preset'
import { setContext } from 'apollo-link-context'
import { createHttpLink } from 'apollo-link-http'

const GRAPHQL_API_ENDPOINT = '/graphql'

const httpLink = createHttpLink({
  uri: GRAPHQL_API_ENDPOINT
})

const authMiddleware = setContext((req, { headers }) => {
  const token = localStorage.getItem('token')
  return {
    headers: {
      ...headers,
      auth: `Bearer ${token}`
    }
  }
})

const client = new ApolloClient({
  link: from([authMiddleware, httpLink]),
  cache: new InMemoryCache()
})

export default client
