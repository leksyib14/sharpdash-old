import { gql } from 'apollo-boost'

export const CREATE_USER = gql`
  mutation CreateUser($email: String!, $username: String) {
    createUser(username: $username, email: $email) {
      user {
        email
        id
        username
      }
      token
    }
  }
`
