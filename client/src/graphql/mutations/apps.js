import { gql } from 'apollo-boost'

export const CREATE_PROJECT = gql`
  mutation CreateProject($name: String!) {
    createApp(name: $name) {
      app {
        id
        name
        apikey
        models
      }
    }
  }
`

export const UPDATE_PROJECT = gql`
  mutation UpdateApp(
    $id: Int!
    $models: [String]
    $plan: String
    $name: String
  ) {
    updateApp(id: $id, models: $models, plan: $plan, name: $name) {
      app {
        models
        name
      }
    }
  }
`

export const REGENERATE_KEY = gql`
  mutation Regenerate($id: Int!) {
    regenerateKey(id: $id) {
      app {
        id
      }
    }
  }
`

export const DELETE_MODEL = gql`
  mutation Delete($id: Int!, $model: String!) {
    deleteModel(id: $id, model: $model) {
      app {
        id
        models
      }
    }
  }
`

export const ADD_TEAM_MEMBER = gql`
  mutation AddTeam($id: Int!, $email: String) {
    addTeammember(id: $id, email: $email) {
      app {
        id
        team
      }
    }
  }
`

export const REMOVE_TEAM_MEMBER = gql`
  mutation RemoveTeam($id: Int!, $email: String) {
    removeTeammember(id: $id, email: $email) {
      app {
        id
        team
      }
    }
  }
`
