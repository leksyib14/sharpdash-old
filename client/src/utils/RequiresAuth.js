import React, { useContext } from 'react'
import { Route, Redirect } from 'react-router-dom'
import user from '../contexts/user'

const AppRoute = ({ component: Component, layout: Layout, ...rest }) => {
  const context = useContext(user)

  if (!context.user) {
    return <Redirect to="/" />
  }
  Layout =
    Layout === undefined
      ? props => <React.Fragment>{props.children}</React.Fragment>
      : Layout

  return (
    <Route
      {...rest}
      render={props => (
        <Layout>
          <Component {...props} />
        </Layout>
      )}
    />
  )
}

export default AppRoute
