export const errorformatter = error =>
  error.replace('GraphQL error: ', '') || error
