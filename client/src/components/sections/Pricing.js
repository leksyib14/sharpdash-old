import React from 'react'
import PropTypes from 'prop-types'
import classNames from 'classnames'
import { SectionTilesProps } from '../../utils/SectionProps'
import SectionHeader from './partials/SectionHeader'
import Button from '../elements/Button'

const propTypes = {
  ...SectionTilesProps.types,
  pricingSwitcher: PropTypes.bool,
  pricingSlider: PropTypes.bool
}

const plans = {
  free: {
    amount: 0,
    plan: null
  },
  startup: {
    amount: 9.99,
    plan: 5245
  }
  // business: {
  //   amount: 22,
  //   plan: 5246
  // }
}

const defaultProps = {
  ...SectionTilesProps.defaults,
  pricingSwitcher: false,
  pricingSlider: false,
  projects: false,
  onSelect: () => {},
  default: null
}

const Pricing = props => {
  const {
    className,
    topOuterDivider,
    bottomOuterDivider,
    topDivider,
    bottomDivider,
    hasBgColor,
    invertColor,
    pushLeft,
    projects,
    onSelect,
    plan
  } = props

  const outerClasses = classNames(
    'pricing section',
    topOuterDivider && 'has-top-divider',
    bottomOuterDivider && 'has-bottom-divider',
    hasBgColor && 'has-bg-color',
    invertColor && 'invert-color',
    className
  )

  const innerClasses = classNames(
    'pricing-inner section-inner',
    topDivider && 'has-top-divider',
    bottomDivider && 'has-bottom-divider'
  )

  const tilesClasses = classNames('tiles-wrap', pushLeft && 'push-left')

  const sectionHeader = {
    title: 'Simple, transparent pricing',
    paragraph: 'We have pricings for every stage of your business!'
  }

  return (
    <section {...props} className={outerClasses}>
      <div className="container">
        <div className={innerClasses}>
          <SectionHeader data={sectionHeader} className="center-content" />
          <div className={tilesClasses}>
            <div className="tiles-item reveal-from-bottom">
              <div className="tiles-item-inner has-shadow">
                <div className="pricing-item-content">
                  <div className="pricing-item-header pb-24 mb-24">
                    <div className="pricing-item-price mb-4">
                      <span className="pricing-item-price-currency h2">$</span>
                      <span className="pricing-item-price-amount h1">0</span>
                      <span className="pricing-item-price-after text-xs">
                        /month
                      </span>
                    </div>
                    <div className="text-xs text-color-low">Free Plan</div>
                  </div>
                  <div className="pricing-item-features mb-40">
                    <div className="pricing-item-features-title h6 text-xs text-color-high mb-24">
                      What’s included
                    </div>
                    <ul className="pricing-item-features-list list-reset text-xs mb-32">
                      <li className="is-checked">Get a SharpDash UI</li>
                      <li className="is-checked">
                        Watch data from 3 db models
                      </li>
                      <li className="is-checked">
                        2000 records limit per model
                      </li>
                      <li className="is-checked">Monthly growth chart</li>
                      <li>No records limit</li>
                      <li>24/7 support</li>
                      <li>No teams</li>
                    </ul>
                  </div>
                </div>
                <div className="pricing-item-cta mb-8">
                  {projects && (
                    <Button
                      color="primary"
                      tag="a"
                      onClick={() => onSelect('free')}
                      disabled={plan === 'free'}
                      wide
                    >
                      {plan === 'free' ? 'Selected' : 'Select'}
                    </Button>
                  )}
                </div>
              </div>
            </div>

            <div
              className="tiles-item reveal-from-bottom"
              data-reveal-delay="200"
            >
              <div className="tiles-item-inner has-shadow">
                <div className="pricing-item-content">
                  <div className="pricing-item-header pb-24 mb-24">
                    <div className="pricing-item-price mb-4">
                      <span className="pricing-item-price-currency h2">$</span>
                      <span className="pricing-item-price-amount h1">
                        {plans.startup.amount}
                      </span>
                      <span className="pricing-item-price-after text-xs">
                        /month
                      </span>
                    </div>
                    <div className="text-xs text-color-low">Startup Plan</div>
                  </div>
                  <div className="pricing-item-features mb-40">
                    <div className="pricing-item-features-title h6 text-xs text-color-high mb-24">
                      What’s included
                    </div>
                    <ul className="pricing-item-features-list list-reset text-xs mb-32">
                      <li className="is-checked">Get a SharpDash UI</li>
                      <li className="is-checked">
                        Watch data from 10 db models
                      </li>
                      <li className="is-checked">
                        40,000 records limit per model
                      </li>
                      <li className="is-checked">Monthly growth chart</li>
                      <li className="is-checked">24/7 support</li>
                      <li className="is-checked">Up to 5 team members</li>
                      <li>No records limit</li>
                    </ul>
                  </div>
                </div>
                <div className="pricing-item-cta mb-8">
                  {projects && (
                    <Button
                      color="primary"
                      tag="a"
                      onClick={() => onSelect('startup')}
                      disabled={plan === 'startup'}
                      wide
                    >
                      {plan === 'startup' ? 'Selected' : 'Upgrade'}
                    </Button>
                  )}
                </div>
              </div>
            </div>

            <div
              className="tiles-item reveal-from-bottom"
              data-reveal-delay="400"
            >
              <div className="tiles-item-inner has-shadow">
                <div className="pricing-item-content">
                  <div className="pricing-item-header pb-24 mb-24">
                    <div className="pricing-item-price mb-4">
                      <span className="pricing-item-price-currency h2">$</span>
                      <span className="pricing-item-price-amount h1">---</span>
                      {/* <span className="pricing-item-price-after text-xs">
                        once
                      </span> */}
                    </div>
                    <div className="text-xs text-color-low">
                      Enterprise plan
                    </div>
                  </div>
                  <div className="pricing-item-features mb-40">
                    <div className="pricing-item-features-title h6 text-xs text-color-high mb-24">
                      What’s included
                    </div>
                    <ul className="pricing-item-features-list list-reset text-xs mb-32">
                      <li className="is-checked">Get a SharpDash UI</li>
                      <li className="is-checked">unlimited db models</li>
                      <li className="is-checked">No records limit</li>
                      <li className="is-checked">Monthly growth chart</li>
                      <li className="is-checked">24/7 support</li>
                      <li className="is-checked">Unlimited team members</li>
                      <li className="is-checked">It's self hosted!</li>
                    </ul>
                  </div>
                </div>
                <div className="pricing-item-cta mb-8">
                  {projects && (
                    <Button
                      color="primary"
                      tag="a"
                      onClick={() =>
                        window.open(
                          'mailto:support@sharpdash.io?subject=I want to discuss the Enterprise plan on SharpDash!'
                        )
                      }
                      wide
                    >
                      Contact support
                    </Button>
                  )}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  )
}

Pricing.propTypes = propTypes
Pricing.defaultProps = defaultProps

export default Pricing
