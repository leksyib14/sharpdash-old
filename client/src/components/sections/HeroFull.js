/* eslint-disable jsx-a11y/accessible-emoji */
import React, { useContext } from 'react'
import classNames from 'classnames'
import { withRouter } from 'react-router-dom'
import user from '../../contexts/user'
import { SectionProps } from '../../utils/SectionProps'
import Button from '../elements/Button'
import Image from '../elements/Image'

const propTypes = {
  ...SectionProps.types
}

const defaultProps = {
  ...SectionProps.defaults
}

const HeroFull = props => {
  const context = useContext(user)
  const {
    className,
    topOuterDivider,
    bottomOuterDivider,
    topDivider,
    bottomDivider,
    hasBgColor,
    invertColor
  } = props

  const outerClasses = classNames(
    'hero section center-content',
    topOuterDivider && 'has-top-divider',
    bottomOuterDivider && 'has-bottom-divider',
    hasBgColor && 'has-bg-color',
    invertColor && 'invert-color',
    className
  )

  const innerClasses = classNames(
    'hero-inner section-inner',
    topDivider && 'has-top-divider',
    bottomDivider && 'has-bottom-divider'
  )

  return (
    <section {...props} className={outerClasses}>
      <div className="container-sm">
        <div className={innerClasses}>
          <div className="hero-content">
            <h1
              className="mt-0 mb-16 reveal-from-bottom"
              data-reveal-delay="200"
            >
              SharpDash ⚡️
            </h1>
            <div className="container-xs">
              <p
                className="m-0 mb-32 reveal-from-bottom"
                data-reveal-delay="400"
              >
                Ready to launch your new app but don't want to spend time
                building a dashboard to monitor data? We got you! With
                SharpDash, you can create a dashboard to monitor your data in
                just 5 minutes!
              </p>
              <div className="reveal-from-bottom" data-reveal-delay="600">
                {context.user ? (
                  <Button
                    tag="button"
                    color="primary"
                    onClick={() => props.history.push('/dashboard')}
                    wideMobile
                  >
                    Dashboard
                  </Button>
                ) : (
                  <Button
                    tag="button"
                    color="primary"
                    onClick={() => props.history.push('/signup')}
                    wideMobile
                  >
                    Get started
                  </Button>
                )}
              </div>
            </div>
          </div>
          <div
            className="hero-figure reveal-from-bottom illustration-element-01"
            data-reveal-value="20px"
            data-reveal-delay="800"
          >
            <Image
              className="has-shadow"
              src={require('../../assets/images/dash.png')}
              style={{ marginTop: '50px' }}
              alt="Hero"
              width={896}
              height={504}
            />
          </div>
        </div>
      </div>
    </section>
  )
}

HeroFull.propTypes = propTypes
HeroFull.defaultProps = defaultProps

export default withRouter(HeroFull)
