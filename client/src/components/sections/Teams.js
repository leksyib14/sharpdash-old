import React, { useState } from 'react'
import { Table } from 'antd'
import { toast } from 'react-toastify'
import Button from '../elements/Button'
import Input from '../elements/Input'
import { useMutation } from '@apollo/react-hooks'
import {
  ADD_TEAM_MEMBER,
  REMOVE_TEAM_MEMBER
} from '../../graphql/mutations/apps'
import { errorformatter } from '../../utils/errorformatter'

// eslint-disable-next-line
const regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/

const Teams = props => {
  const [email, setEmail] = useState('')

  const team = (props?.team || []).map(item => ({
    key: item,
    email: item
  }))

  const onchange = e => {
    setEmail(e.target.value.trim())
  }

  const [addTeam, { loading, error }] = useMutation(ADD_TEAM_MEMBER)

  const [removeTeam, { error: removeError }] = useMutation(REMOVE_TEAM_MEMBER)

  const removeItem = async email => {
    await removeTeam({ variables: { id: Number(props?.appId), email } })
    props.refetch()
  }

  const columns = [
    {
      title: 'email',
      dataIndex: 'email',
      width: '70%',
      editable: true
    },
    {
      title: 'Action',
      dataIndex: 'operation',
      key: 'email',
      render: (e, r, i) => (
        <button
          class="button button-sm"
          onClick={() => removeItem(r.email)}
          style={{
            background: 'transparent',
            padding: 0,
            boxShadow: 'none',
            color: 'red'
          }}
        >
          Delete
        </button>
      )
    }
  ]

  const mergedcolumns = columns.map(col => {
    return {
      ...col,
      onCell: record => ({
        record,
        dataIndex: col.dataIndex,
        title: col.title
      })
    }
  })

  if (error || removeError) {
    toast.error(errorformatter(error?.message || removeError?.message))
  }

  const completeAddTeam = async () => {
    if (!regex.test(email)) {
      toast.error('Email is invalid')
      return
    }
    await addTeam({ variables: { id: Number(props?.appId), email } })
    props.refetch()
  }

  return (
    <section>
      <div className="container">
        <h2 style={{ textAlign: 'center' }}>Invite teammate</h2>
        <div
          className="form-group mt-8 mb-32"
          style={{
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            flexWrap: 'wrap'
          }}
        >
          <Input
            type="text"
            className="class-input p-8"
            formGroup="desktop"
            onChange={onchange}
            labelHidden
            placeholder="Enter email address"
          >
            <Button
              color="primary"
              loading={loading}
              onClick={() => completeAddTeam()}
            >
              Invite
            </Button>
          </Input>
        </div>
        <Table dataSource={team} columns={mergedcolumns} />
      </div>
    </section>
  )
}

export default Teams
