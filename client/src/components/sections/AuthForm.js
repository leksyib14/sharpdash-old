import React, { useContext } from 'react'
import classNames from 'classnames'
import { SectionProps } from '../../utils/SectionProps'
import { Link } from 'react-router-dom'
import SectionHeader from './partials/SectionHeader'
import { useMutation } from '@apollo/react-hooks'
import usercontext from '../../contexts/user'
import Button from '../elements/Button'
import { withRouter } from 'react-router-dom'
import GoogleLogin from 'react-google-login'
import { CREATE_USER } from '../../graphql/mutations/user'
import { toast } from 'react-toastify'
import { errorformatter } from '../../utils/errorformatter'

const propTypes = {
  ...SectionProps.types
}

const defaultProps = {
  ...SectionProps.defaults
}

const SignupForm = props => {
  const context = useContext(usercontext)
  const {
    className,
    topOuterDivider,
    bottomOuterDivider,
    topDivider,
    bottomDivider,
    hasBgColor,
    invertColor
  } = props

  const [createUser, { data, loading, error }] = useMutation(CREATE_USER)

  const outerClasses = classNames(
    'signin section',
    topOuterDivider && 'has-top-divider',
    bottomOuterDivider && 'has-bottom-divider',
    hasBgColor && 'has-bg-color',
    invertColor && 'invert-color',
    className
  )

  const innerClasses = classNames(
    'signin-inner section-inner',
    topDivider && 'has-top-divider',
    bottomDivider && 'has-bottom-divider'
  )

  const sectionHeader = {
    title:
      (window.location.pathname === '/signin' ? 'Sign in' : 'Sign up') +
      ' to use SharpDash'
  }

  if (error) {
    toast.error(errorformatter(error?.message))
  }

  if (data) {
    localStorage.setItem('token', data.createUser.token)
    localStorage.setItem('user', JSON.stringify(data.createUser.user))
    context.setUser()
    toast('Welcome!', { type: 'success' })
    props.history.push('/dashboard')
  }

  return (
    <section {...props} className={outerClasses}>
      <div className="container">
        <div className={innerClasses}>
          <SectionHeader
            tag="h1"
            data={sectionHeader}
            className="center-content"
          />
          <div className="tiles-wrap">
            <div className="tiles-item">
              <div className="tiles-item-inner">
                <form>
                  <fieldset>
                    <div className="mt-24 mb-32">
                      <GoogleLogin
                        clientId="574133083518-2ardpt1g8anag58h3ssghe1sni34stg6.apps.googleusercontent.com"
                        render={renderProps => (
                          <Button
                            tag="button"
                            style={{
                              background: 'transparent',
                              border: '1px solid white',
                              color: 'white'
                            }}
                            wide
                            loading={loading}
                            onClick={renderProps.onClick}
                            disabled={renderProps.disabled}
                          >
                            <img
                              src="https://img.icons8.com/color/48/000000/google-logo.png"
                              width="23px"
                              height="24px"
                              className="mr-8"
                              alt="google"
                            />
                            {(window.location.pathname === '/signin'
                              ? 'Sign in'
                              : 'Sign up') + ' with Google'}
                          </Button>
                        )}
                        buttonText="Login"
                        onSuccess={s =>
                          createUser({
                            variables: {
                              email: s.profileObj.email,
                              username: s.profileObj.givenName
                            }
                          })
                        }
                        onFailure={e => console.log(e)}
                        cookiePolicy={'single_host_origin'}
                      />
                    </div>
                  </fieldset>
                  <fieldset>
                    <div className="mt-24 mb-32 tooltip">
                      <Button
                        style={{
                          background: 'white',
                          color: 'black'
                        }}
                        wide
                        disabled
                      >
                        <img
                          src="https://img.icons8.com/material-outlined/24/000000/github.png"
                          width="23px"
                          height="24px"
                          alt="github"
                          className="mr-8"
                        />
                        {(window.location.pathname === '/signin'
                          ? 'Sign in'
                          : 'Sign up') + ' with Github'}
                      </Button>
                      <span class="tooltiptext">Coming soon</span>
                    </div>
                  </fieldset>
                </form>
                <div
                  style={{
                    cursor: 'pointer',
                    textAlign: 'center',
                    marginTop: '20px',
                    marginBottom: '20px',
                    display: 'inline-block'
                  }}
                  onClick={e => {
                    e.preventDefault()
                    props.history.goBack()
                  }}
                >
                  Go Back
                </div>
                {window.location.pathname === '/signup' && (
                  <div className="signin-bottom has-top-divider">
                    <div className="pt-32 text-xs center-content text-color-low">
                      By signing up you agree to our
                      <Link to="/privacy-policy" className="func-link">
                        {' '}
                        Privacy policy
                      </Link>
                    </div>
                  </div>
                )}
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  )
}

SignupForm.propTypes = propTypes
SignupForm.defaultProps = defaultProps

export default withRouter(SignupForm)
