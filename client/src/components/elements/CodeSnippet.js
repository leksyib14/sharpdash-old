import React, { useEffect } from 'react'
import SyntaxHighlighter from 'react-syntax-highlighter'
import { docco } from 'react-syntax-highlighter/dist/esm/styles/hljs'

const CodeSnippet = props => {
  useEffect(() => {
    const nodes = [...document.querySelectorAll('pre')]
    nodes.forEach(element => {
      element.setAttribute(
        'style',
        'display: block; overflow-x: auto; padding: 0.5em;'
      )
    })
  }, [])

  return (
    <SyntaxHighlighter language="javascript" style={docco} showLineNumbers>
      {props.codeString}
    </SyntaxHighlighter>
  )
}

export default CodeSnippet
