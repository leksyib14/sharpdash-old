import React from 'react'
import classNames from 'classnames'
import Button from './Button'
import Image from './Image'

const DashboardHero = ({ className, projects, openModal }) => {
  const classes = classNames('dash-hero', className)

  const noProjects = projects.length < 1
  return (
    <div className={classes}>
      <div className="text-col">
        <h2 className="h1-no-margin">Hello!</h2>
        <p className="h1-no-margin white">
          {noProjects
            ? 'Welcome to SharpDash ⚡️'
            : 'See all your projects below'}
        </p>
        {noProjects && (
          <Button
            tag="button"
            wideMobile
            className="btn-hero"
            size="lg"
            onClick={openModal}
          >
            Create New Project
          </Button>
        )}
      </div>
      <Image
        src={require('../../assets/images/dashhero.svg')}
        className="dash-hero__illustration"
      />
    </div>
  )
}

export default DashboardHero
