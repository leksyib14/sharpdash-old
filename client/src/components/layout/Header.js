/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { useState, useEffect, useContext } from 'react'
import PropTypes from 'prop-types'
import { withRouter } from 'react-router-dom'
import { useApolloClient } from '@apollo/react-hooks'
import user from '../../contexts/user'
import Button from '../elements/Button'
import classNames from 'classnames'
import Logo from './partials/Logo'

const propTypes = {
  active: PropTypes.bool,
  navPosition: PropTypes.string,
  hideNav: PropTypes.bool,
  hideSignin: PropTypes.bool,
  bottomOuterDivider: PropTypes.bool,
  bottomDivider: PropTypes.bool
}

const defaultProps = {
  active: false,
  navPosition: '',
  hideNav: false,
  hideSignin: false,
  bottomOuterDivider: false,
  bottomDivider: false
}

const Header = props => {
  const context = useContext(user)

  const client = useApolloClient()

  const [isActive, setisActive] = useState(false)

  const nav = React.createRef()
  const hamburger = React.createRef()

  useEffect(() => {
    props.active && openMenu()
    document.addEventListener('keydown', keyPress)
    document.addEventListener('click', clickOutside)

    return () => {
      document.removeEventListener('keydown', keyPress)
      document.addEventListener('click', clickOutside)
      closeMenu()
    }
  }, [])

  const openMenu = () => {
    document.body.classList.add('off-nav-is-active')
    nav.current.style.maxHeight = nav.current.scrollHeight + 'px'
    setisActive(true)
  }

  const closeMenu = () => {
    document.body.classList.remove('off-nav-is-active')
    nav.current && (nav.current.style.maxHeight = null)
    setisActive(false)
  }

  const keyPress = e => {
    isActive && e.keyCode === 27 && closeMenu()
  }

  const clickOutside = e => {
    if (!nav.current) return
    if (
      !isActive ||
      nav.current.contains(e.target) ||
      e.target === hamburger.current
    )
      return
    closeMenu()
  }
  const {
    className,
    navPosition,
    hideNav,
    hideSignin,
    bottomOuterDivider,
    bottomDivider
  } = props

  const classes = classNames(
    'site-header',
    bottomOuterDivider && 'has-bottom-divider',
    className
  )

  return (
    <header {...props} className={classes}>
      <div className="container">
        <div
          className={classNames(
            'site-header-inner',
            bottomDivider && 'has-bottom-divider'
          )}
        >
          <Logo />
          {!hideNav && (
            <React.Fragment>
              <button
                ref={hamburger}
                className="header-nav-toggle"
                onClick={isActive ? closeMenu : openMenu}
              >
                <span className="screen-reader">Menu</span>
                <span className="hamburger">
                  <span className="hamburger-inner"></span>
                </span>
              </button>
              <nav
                ref={nav}
                className={classNames('header-nav', isActive && 'is-active')}
              >
                <div className="header-nav-inner">
                  {!context.user && (
                    <ul
                      className={classNames(
                        'list-reset text-xxs',
                        navPosition && `header-nav-${navPosition}`
                      )}
                    >
                      <li>
                        <a
                          style={{ cursor: 'pointer' }}
                          href="#"
                          onClick={e => {
                            e.preventDefault()
                            props.history.push('/signin')
                          }}
                        >
                          Sign in
                        </a>
                      </li>
                    </ul>
                  )}

                  {!hideSignin && (
                    <ul className="list-reset header-nav-right">
                      {!context.user && (
                        <li>
                          <Button
                            tag="button"
                            color="primary"
                            className="button button-primary button-wide-mobile button-sm"
                            wideMobile
                            onClick={e => {
                              e.preventDefault()
                              props.history.push('/signup')
                            }}
                          >
                            Sign up
                          </Button>
                        </li>
                      )}
                      {context.user && (
                        <Button
                          tag="button"
                          color="primary"
                          className="button button-primary button-wide-mobile button-sm"
                          wideMobile
                          onClick={() => props.history.push('/dashboard')}
                        >
                          Dashboard
                        </Button>
                      )}
                    </ul>
                  )}

                  {context.user && (
                    <ul
                      className={classNames(
                        'list-reset text-xxs',
                        navPosition && `header-nav-${navPosition}`
                      )}
                    >
                      <li>
                        <a
                          style={{ cursor: 'pointer' }}
                          href="#"
                          onClick={() => {
                            localStorage.removeItem('user')
                            localStorage.removeItem('token')
                            context.setUser()
                            client.resetStore()
                          }}
                        >
                          Sign out
                        </a>
                      </li>
                    </ul>
                  )}
                </div>
              </nav>
            </React.Fragment>
          )}
        </div>
      </div>
    </header>
  )
}

Header.propTypes = propTypes
Header.defaultProps = defaultProps

export default withRouter(Header)
