/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, useEffect } from 'react'
import classNames from 'classnames'
import { useQuery, useMutation } from '@apollo/react-hooks'
import Loader from 'react-loader-spinner'
import { GET_USER_PROJECTS } from '../graphql/queries/apps'
import { CREATE_PROJECT, UPDATE_PROJECT } from '../graphql/mutations/apps'
import Image from '../components/elements/Image'
import Input from '../components/elements/Input'
import Modal from '../components/elements/Modal'
import Button from '../components/elements/Button'
import DashboardHero from '../components/elements/DashboardHero'
import { toast } from 'react-toastify'
import { errorformatter } from '../utils/errorformatter'

const Dashboard = props => {
  const [name, setName] = useState('')
  const [nameIsInvalid, setValidity] = useState(null)
  const [modalActive, setModal] = useState(false)
  const [modal2Active, setModal2] = useState({ active: false })
  const outerClasses = classNames('signin container section')
  const { error, data, loading, refetch } = useQuery(GET_USER_PROJECTS)

  const [
    createProject,
    { loading: createLoading, error: createError, data: createData }
  ] = useMutation(CREATE_PROJECT)

  const [
    updateProject,
    { loading: updateLoading, error: updateError, data: updateData }
  ] = useMutation(UPDATE_PROJECT)

  if (error || createError || updateError) {
    const err = error || createError || updateError
    toast.error(errorformatter(err.message))
  }

  const projects = data?.getUserApps.apps

  const onchange = e => {
    setName(e.target.value)
  }

  const completeProjectCreation = async (type = 'create', id) => {
    if (!name) {
      return setValidity('Project name is required')
    } else if (name.length < 3) {
      return setValidity('Project name should have at least 3 characters')
    } else if (name.length > 25) {
      return setValidity('Project name cannot be more than 25 characters')
    }

    if (type === 'create') {
      await createProject({ variables: { name } })
      toast.success('Project created successfully!')
    } else {
      await updateProject({ variables: { name, id } })
      toast.success('Project renamed successfully!')
    }

    setName('')

    setModal(false)
    setModal2({ active: false })
  }

  useEffect(() => {
    refetch()
  }, [createData, updateData])

  return (
    <React.Fragment>
      <section className={outerClasses}>
        <DashboardHero
          projects={projects || []}
          openModal={() => setModal(true)}
        />
        {loading && (
          <div className="loader">
            <Loader
              type="Puff"
              color={'#4A61DD'}
              height={50}
              width={50}
              visible={loading}
            />
          </div>
        )}
        {/* PROJECTS SECTIONs */}
        <div className="projects">
          {/* // no projects */}
          {projects && projects?.length < 1 && (
            <div className="projects__empty-state">
              <h4 className="title">Create a new project to get started</h4>
              <h6 className="subtitle">
                Track all your projects in a single dashboard
              </h6>
            </div>
          )}

          {/* projects available */}
          {projects && projects?.length > 0 && (
            <React.Fragment>
              <ul className="projects__list">
                {/* add project */}
                <li
                  className="project create-new"
                  onClick={() => setModal(true)}
                >
                  <Image src={require('../assets/images/plus.svg')} />
                  <div className="create-new__text">Create new project</div>
                </li>
                {/* existing projects */}
                {projects.map(project => (
                  <li className="project" key={project.id}>
                    <h6 className="project__name">
                      {project.name}{' '}
                      <div className="project__plan">({project.plan})</div>
                    </h6>
                    <div className="project__models">
                      <b>{project?.models?.length || 0}</b> model
                      {project?.models?.length > 1 && 's'}
                    </div>
                    <div
                      style={{
                        display: 'flex',
                        justifyContent: 'space-between',
                        width: '100%',
                        marginTop: '20px'
                      }}
                    >
                      <Button
                        style={{ padding: '4px 10px' }}
                        class="button button-sm"
                        size="sm"
                        onClick={() =>
                          props.history.push(
                            `/dashboard/projects/${project.id}`
                          )
                        }
                      >
                        VIEW PROJECT
                      </Button>
                      <Button
                        style={{
                          background: 'transparent',
                          boxShadow: 'none',
                          color: '#444'
                        }}
                        class="button button-sm"
                        size="sm"
                        onClick={() =>
                          setModal2({
                            ...modal2Active,
                            active: true,
                            data: { id: project.id, name: project.name }
                          })
                        }
                      >
                        RENAME
                      </Button>
                    </div>
                  </li>
                ))}
              </ul>
            </React.Fragment>
          )}
        </div>

        <Modal
          id="demo-modal"
          show={modalActive}
          handleClose={() => setModal(false)}
        >
          <div className="center-content">
            <h3 className="mt-0 mb-12">Create A New Project</h3>
            <p className="text-sm">
              Fill in the required details to create your project.
            </p>
          </div>
          <div>
            <Input
              type="text"
              label="Enter your project's name"
              placeholder="Enter your project's name"
              formGroup="desktop"
              labelHidden
              status={nameIsInvalid ? 'error' : null}
              hint={nameIsInvalid}
              onChange={onchange}
              onKeyPress={event =>
                event.key === 'Enter' && completeProjectCreation()
              }
            >
              <Button
                color="primary"
                loading={createLoading}
                onClick={() => completeProjectCreation()}
              >
                Submit
              </Button>
            </Input>
          </div>
        </Modal>
        <Modal
          id="demo-modal"
          show={modal2Active.active}
          handleClose={() => setModal2({ active: false })}
        >
          <div className="center-content">
            <h3 className="mt-0 mb-12">Rename Project</h3>
            <p className="text-sm">Enter the new name for this project.</p>
          </div>
          <div>
            <Input
              type="text"
              label="Enter your project's name"
              placeholder="Enter your project's new name"
              formGroup="desktop"
              labelHidden
              status={nameIsInvalid ? 'error' : null}
              hint={nameIsInvalid}
              onChange={onchange}
              defaultValue={modal2Active?.data?.name}
              onKeyPress={event =>
                event.key === 'Enter' &&
                completeProjectCreation('edit', Number(modal2Active?.data.id))
              }
            >
              <Button
                color="primary"
                loading={updateLoading}
                onClick={() =>
                  completeProjectCreation('edit', Number(modal2Active?.data.id))
                }
              >
                Submit
              </Button>
            </Input>
          </div>
        </Modal>
      </section>
    </React.Fragment>
  )
}

export default Dashboard
