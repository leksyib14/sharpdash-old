import React from 'react'

const NotFound = () => {
  return (
    <section className="container section">
      <div className="projects__empty-state">
        <h4 className="title">404 Page not found.</h4>
        <h6 className="subtitle">
          Maybe you missed that link, but no problem!
        </h6>
      </div>
    </section>
  )
}

export default NotFound
