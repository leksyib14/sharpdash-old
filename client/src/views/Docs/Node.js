import React from 'react'
import CodeSnippet from '../../components/elements/CodeSnippet'
import { Link } from 'react-router-dom'

const NodeDoc = () => {
  return (
    <React.Fragment>
      <div className="container section">
        <h2 className="mt-0">NodeJS quick start</h2>
        <p>
          After following this guide you would have created and finished setting
          up your dashboard.{' '}
        </p>

        <h4>1. Get your API key</h4>
        <p>
          <Link to="/dashboard">Create a SharpDash project</Link>, click the
          "create new model" button to add the name of a database model you want
          to track. This is just the name of the database model you want to
          track. You can also add multiple, but in this quickstart, we will be
          using just one which is "users", to track the "users" table of our
          application. After adding these models, go to the "API" tab in your
          project's page and copy your API key. You can also notice there's a
          "regenerate" button for you to regenerate an API key if it gets
          compromised.
        </p>

        <h4>2. Install the sharpdash sdk.</h4>
        <p>
          Use yarn or npm to install this package.
          <br />
          <code lang="javascript">
            npm install sharpdashboard-sdk-node
            <br />
            or
            <br />
            yarn add sharpdashboard-sdk-node
          </code>
        </p>

        <h4>3. Setup a NodeJS server</h4>
        <p>
          Use yarn or npm to install this package. we'll use express and
          PostgreSQL (sequelize) here but you can count on SharpDash to work
          with any Node stack!
          <br />
        </p>
        <CodeSnippet
          codeString={`
  // app.js
  const express = require("express");
  const bodyParser = require("body-parser");
  const app = express();

  app.use(bodyParser.urlencoded({ extended: true }));
  app.use(bodyParser.json());

  app.listen(4000, () => 'Server is listening on 4000!')
          `}
        />

        <h4>3. Create a SharpDash helper file</h4>
        <p>
          What SharpDash does is to
          <br />
          <br />
          <ul>
            <li>Get your data.</li>
            <li>Cache your data and make it ready for you.</li>
            <li>Send your data down to your dashboard for your access.</li>
          </ul>
          NOTE: SharpDash does NOT store your data. It only caches it and sends
          it to you when you need it.
          <br />
          In this step we're going to initialize the SharpDash sdk then write a
          function that gets data (from your database/datasource) and sends the
          data you want to monitor to SharpDash.
          <br />
          <br />
          Now let's create a `sharpdash.js` file:
        </p>
        <CodeSnippet
          codeString={`
  // sharpdash.js
  const { SharpDashBoard } = require('sharpdashboard-sdk-node')
  const { Users } = require('../models')

  // function to get all your users for the "users" model we created earlier
  // in this example, we're using a Postgresql database with sequelize.
  // You can use any database as long as it returns iterable (array) data.
  // NOTE: make sure to exclude other fields you don't need to see, like passwords or other sort of tokens.

  const getUsers = async () => {
    try {
      const users = await User.findAll({ raw: true })
      return users
    } catch (error) {
      console.log(error.message)
    }
  }

  // initialize a new Sharpdashboard instance with your API key
  const dashboard = new SharpDashBoard('api_key_here')

  // this is a function to get your data to your dashboard.
  // the first parameter is a model (as created in step 1), second is a function to get data required for that model.
  // NOTE: make sure the model name you're supplying as a first argument in this function has been created as in step 1.
  dashboard.addModelHandler('users', getUsers)

  export default dashboard
          `}
        />
        <h4>4. Import the setup in your root file</h4>
        <p>
          The last step is to import the sharpdash config file in your root file
          so it will run when your server starts.
          <br />
        </p>

        <CodeSnippet
          codeString={`
  // app.js (root file)
  const express = require("express");
  const bodyParser = require("body-parser");
  require('./sharpdash.js')

  const app = express();

  app.use(bodyParser.urlencoded({ extended: true }));
  app.use(bodyParser.json());

  app.listen(4000, () => 'Server is listening on 4000!')
          `}
        />

        <h4>5. That's all!</h4>
        <p>Now check your dashboard and monitor your data from there.</p>
      </div>
    </React.Fragment>
  )
}

export default NodeDoc
