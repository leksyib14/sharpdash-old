import React from 'react'
import HeroFull from '../components/sections/HeroFull'
import FeaturesTiles from '../components/sections/FeaturesTiles'

import Pricing from '../components/sections/Pricing'
import Cta from '../components/sections/Cta'

const Home = () => {
  return (
    <React.Fragment>
      <HeroFull className="illustration-section-01" />
      <FeaturesTiles />
      {/* <FeaturesSplit invertMobile topDivider imageFill /> */}
      <Pricing />
      <Cta split />
    </React.Fragment>
  )
}

export default Home
