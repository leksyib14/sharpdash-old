import React, { useState, useContext, useEffect } from 'react'
import { Link } from 'react-router-dom'
import classNames from 'classnames'
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs'
import Image from '../components/elements/Image'
import Button from '../components/elements/Button'
import { toast } from 'react-toastify'
import 'react-tabs/style/react-tabs.css'
import { useRavePayment } from 'react-ravepayment'
import { useParams } from 'react-router-dom'
import user from '../contexts/user'
import {
  UPDATE_PROJECT,
  REGENERATE_KEY,
  DELETE_MODEL
} from '../graphql/mutations/apps'
import Input from '../components/elements/Input'
import Modal from '../components/elements/Modal'
import Pricing from '../components/sections/Pricing'
import Teams from '../components/sections/Teams'
import Loader from 'react-loader-spinner'
import { useQuery, useMutation } from '@apollo/react-hooks'
import { GET_PROJECT_DETAILS } from '../graphql/queries/apps'
import { CopyToClipboard } from 'react-copy-to-clipboard'
import { errorformatter } from '../utils/errorformatter'

const plans = {
  free: {
    amount: 0,
    plan: null
  },
  startup: {
    amount: 9.99,
    plan: 5245
  },
  business: {
    amount: 22,
    plan: 5246
  }
}

const icons = [
  require('../assets/images/stat1.svg'),
  require('../assets/images/stat2.svg'),
  require('../assets/images/stat3.svg')
]

function numberWithCommas(x) {
  return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',')
}

const Project = props => {
  const outerClasses = classNames('signin container section')

  const context = useContext(user)

  const params = useParams()
  const [name, setName] = useState('')
  const [nameIsInvalid, setValidity] = useState(null)
  const [modalActive, setModal] = useState(false)
  const [modal2Active, setModal2] = useState({ active: false })

  const { data, loading, error, refetch } = useQuery(GET_PROJECT_DETAILS, {
    variables: { id: Number(params.id) || 0 },
    pollInterval: 5000
  })

  const onchange = e => {
    setName(e.target.value)
  }

  const [
    updateProject,
    { loading: updateLoading, error: updateError }
  ] = useMutation(UPDATE_PROJECT)

  const [
    deleteModel,
    { loading: deleteModelLoading, error: deleteModelError }
  ] = useMutation(DELETE_MODEL)

  const [
    regenerateKey,
    { data: regenerateData, error: regenerateError }
  ] = useMutation(REGENERATE_KEY)

  if (updateError || regenerateError || deleteModelError) {
    toast.error(
      errorformatter(
        updateError?.message ||
          regenerateError?.message ||
          deleteModelError?.message
      )
    )
  }

  if (regenerateData) {
    refetch()
    toast.success('API key regenerated!')
  }

  const completeProjectUpdate = async () => {
    if (!name) {
      return setValidity('Model name is required')
    } else if (name.length < 3) {
      return setValidity('Model name should have at least 3 characters')
    } else if (name.length > 25) {
      return setValidity('Model name cannot be more than 25 characters')
    }

    await updateProject({
      variables: { models: [name.toLowerCase()], id: Number(params.id) }
    })
    toast.success('New model added successfully')
    refetch()
    setModal(false)
  }

  const completeModelDelete = async (id, model) => {
    await deleteModel({ variables: { id, model } })
    setModal2({ active: false })
    refetch()
    return
  }

  const project = data?.getAppDetails.data

  const models = project?.models || []

  const [plan, setPlan] = useState(null)

  const { initializePayment } = useRavePayment({
    txref: `${project?.id}-${project?.name}`,
    customer_email: context.user.email,
    amount: plans[plan]?.amount || 0,
    PBFPubKey: 'FLWPUBK-bb173d555513f541be295e77c0302f59-X',
    production: true,
    currency: 'USD',
    customer_firstname: context.user.username,
    customer_lastname: context.user.username,
    payment_plan: plans[plan]?.plan || 0
  })

  useEffect(() => {
    if (plan !== 'free') {
      initializePayment(
        d => upgradePlan(d),
        err => toast.error(err?.message || 'An error occured')
      )
    } else if (plan === 'free') {
      updateProject({ variables: { plan: 'free', id: Number(project?.id) } })
        .then(i => refetch())
        .then(() => toast.success('unsubscribed successfully!'))
    }

    // eslint-disable-next-line
  }, [plan])

  const upgradePlan = async transactionDetails => {
    if (transactionDetails.data.data.status === 'successful') {
      await updateProject({ variables: { plan, id: Number(project?.id) } })
      document.location.reload()
    }
  }

  const onCopy = () => {
    toast.info('Copied to clipboard', {
      position: 'top-right',
      autoClose: 3000,
      hideProgressBar: true,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined
    })
  }

  return (
    <React.Fragment>
      <section className={outerClasses}>
        <div
          style={{
            cursor: 'pointer',
            marginTop: '40px',
            marginBottom: '20px',
            display: 'inline-block'
          }}
          onClick={e => {
            e.preventDefault()
            props.history.goBack()
          }}
        >
          back
        </div>

        <h5 className="project-heading">
          <span>{project?.name}</span>{' '}
          <span style={{ textTransform: 'uppercase' }}>
            {project?.name && (
              <span className="tablabel">({project?.plan})</span>
            )}
          </span>
        </h5>

        <Tabs className="projects-tab" selectedTabClassName="selected-tab">
          <TabList className="tablist">
            <Tab
              disabled={error || loading ? true : false}
              className="react-tabs__tab tablabel"
            >
              Product Overview
            </Tab>
            <Tab
              disabled={error || loading ? true : false}
              className="react-tabs__tab tablabel"
            >
              API
            </Tab>
            {['business', 'startup'].includes(project?.plan) && (
              <Tab
                disabled={error || loading ? true : false}
                className="react-tabs__tab tablabel"
              >
                Teams
              </Tab>
            )}
            <Tab
              disabled={error || loading ? true : false}
              className="react-tabs__tab tablabel"
            >
              Upgrade Plan
            </Tab>
          </TabList>
          {/* product overview tap panel */}
          <TabPanel>
            {models.length > 0 && (
              <Button className="new-model" onClick={() => setModal(true)}>
                Create a new model
              </Button>
            )}
            {loading && (
              <div className="loader">
                <Loader
                  type="Puff"
                  color={'#4A61DD'}
                  height={50}
                  width={50}
                  visible={loading}
                />
              </div>
            )}
            {error && (
              <div className="projects__empty-state">
                <h4 className="title">App not found</h4>
                <h6 className="subtitle">
                  Maybe you missed that link, but no problem!
                </h6>
                <Button onClick={() => props.history.push('/dashboard')}>
                  Create your first project
                </Button>
              </div>
            )}
            {!error && !loading && models.length < 1 && (
              <div className="projects__empty-state">
                <h4 className="title">Create a model to continue</h4>
                <h6 className="subtitle">
                  The next step is to create a model and link SharpDash with
                  that model.
                </h6>
                <Button onClick={() => setModal(true)}>
                  Create your first model
                </Button>
              </div>
            )}
            <div className="models">
              {!error && models.length > 0 && (
                <React.Fragment>
                  <p className="h1-no-margin sectionTitle">Model Count</p>
                  <ul className="models__list">
                    {models &&
                      project.models.map((p, index) => (
                        <li className="model" key={index}>
                          <Image src={icons[index] || icons[2]} />
                          <div className="model__name">{p}</div>
                          <div className="model__count">
                            {numberWithCommas(project[p])}
                          </div>
                          <div
                            style={{
                              display: 'flex',
                              justifyContent: 'space-between',
                              width: '100%',
                              marginTop: '10px'
                            }}
                          >
                            <Button
                              style={{ padding: '4px 10px' }}
                              className="button button-sm"
                              size="sm"
                              onClick={() =>
                                props.history.push(
                                  `/dashboard/projects/${project.id}/model/${p}`
                                )
                              }
                            >
                              VIEW MODEL
                            </Button>
                            <Button
                              size="sm"
                              style={{
                                background: 'transparent',
                                boxShadow: 'none',
                                color: 'red'
                              }}
                              className="button button-sm"
                              onClick={() =>
                                setModal2({
                                  active: true,
                                  data: { id: project?.id, model: p }
                                })
                              }
                            >
                              DELETE
                            </Button>
                          </div>
                        </li>
                      ))}
                  </ul>
                </React.Fragment>
              )}
            </div>
          </TabPanel>
          <TabPanel>
            <div
              style={{
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                flexWrap: 'wrap'
              }}
              className="mb-32"
            >
              <div className="form-group mt-12">
                <input
                  type="text"
                  className="class-input p-4"
                  size="sm"
                  value={project?.apikey}
                  disabled
                  placeholder="API Key"
                />
                <CopyToClipboard onCopy={onCopy} text={project?.apikey}>
                  <button className="button button-primary mr-16">
                    <Image
                      src={require('../assets/images/copy-icon.svg')}
                      alt="Copy"
                      className="mr-8"
                      width="16px"
                    />
                    COPY
                  </button>
                </CopyToClipboard>
              </div>
              <button
                className="button button-dark mt-12"
                onClick={() =>
                  regenerateKey({ variables: { id: Number(project.id) } })
                }
              >
                <Image
                  src={require('../assets/images/regenerate-icon.svg')}
                  alt="Regenerate"
                  className="mr-8"
                  width="16px"
                />
                Regenerate Key
              </button>
            </div>
            <div className="pt-32 has-top-divider">
              <b>API Docs</b>
              <ul>
                <li>
                  <Link to="/docs/node">NodeJS</Link>
                </li>
              </ul>
            </div>
          </TabPanel>
          {/* teams */}
          {['business', 'startup'].includes(project?.plan) && (
            <TabPanel>
              <Teams
                team={project?.team}
                refetch={refetch}
                appId={project?.id}
              />
            </TabPanel>
          )}
          {/* UPGRADE PLAN */}
          <TabPanel>
            <Pricing projects plan={project?.plan} onSelect={i => setPlan(i)} />
          </TabPanel>
        </Tabs>
        <Modal
          id="demo-modal"
          show={modalActive}
          handleClose={() => setModal(false)}
        >
          <div className="center-content">
            <h3 className="mt-0 mb-12">Create A New Model</h3>
            <p className="text-sm">Add your new database model's name</p>
          </div>
          <div>
            <Input
              type="text"
              label="Enter your new model's name"
              placeholder="Enter your new model's name"
              formGroup="desktop"
              labelHidden
              status={nameIsInvalid ? 'error' : null}
              hint={nameIsInvalid}
              onChange={onchange}
              onKeyPress={event =>
                event.key === 'Enter' && completeProjectUpdate()
              }
            >
              <Button
                color="primary"
                loading={updateLoading}
                onClick={() => completeProjectUpdate()}
              >
                Submit
              </Button>
            </Input>
          </div>
        </Modal>
        <Modal
          id="demo-modal"
          show={modal2Active.active}
          handleClose={() => setModal2({ active: false })}
        >
          <div className="center-content">
            <h3 className="mt-0 mb-12">Delete Model</h3>
            <p className="text-sm">
              Are you sure you want to delete this model?
            </p>
            <div>
              <Button
                loading={deleteModelLoading}
                onClick={() =>
                  completeModelDelete(
                    Number(project?.id),
                    modal2Active.data?.model
                  )
                }
              >
                DELETE
              </Button>
            </div>
          </div>
        </Modal>
      </section>
    </React.Fragment>
  )
}

export default Project
