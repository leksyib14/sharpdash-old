import React, { useState, useEffect, useRef } from 'react'
import { Table } from 'antd'
import { useQuery } from '@apollo/react-hooks'
import { useParams } from 'react-router-dom'
import { GET_MODEL_DATA } from '../graphql/queries/apps'
import {
  BarChart,
  Bar,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
  ResponsiveContainer
} from 'recharts'
import Input from '../components/elements/Input'
import ResizeObserver from 'rc-resize-observer'
import Button from '../components/elements/Button'
import 'antd/dist/antd.css'

const numberOfItemsPerMonth = (data, dateField = 'createdAt') => {
  const hasCreatedAt = data[0]?.[dateField] || data[1]?.[dateField]
  if (hasCreatedAt) {
    const months = [
      'Jan',
      'Feb',
      'Mar',
      'Apr',
      'May',
      'Jun',
      'Jul',
      'Aug',
      'Sept',
      'Oct',
      'Nov',
      'Dec'
    ]
    const dataPerMonth = months.map(i => ({ month: i, count: 0 }))
    data.forEach(item => {
      const date = new Date(item?.createdAt)
      if (date !== 'Invalid Date') {
        const month = date.getMonth()
        const currentCountForMonth = dataPerMonth[month].count
        dataPerMonth[month].count = currentCountForMonth + 1
      }
    })
    return dataPerMonth
  } else return []
}

function isCyclic(obj) {
  let seenObjects = []

  function detect(obj) {
    if (obj && typeof obj === 'object') {
      if (seenObjects.indexOf(obj) !== -1) {
        return true
      }
      seenObjects.push(obj)
      for (let key in obj) {
        if (obj.hasOwnProperty(key) && detect(obj[key])) {
          return true
        }
      }
    }
    return false
  }

  return detect(obj)
}

const Details = props => {
  const [dateField, setDateField] = useState('createdAt')
  const [tableWidth, setTableWidth] = useState(0)

  const [pageSize, setPageSize] = useState(10)
  const gridRef = useRef()

  useEffect(() => resetVirtualGrid, [])
  useEffect(() => resetVirtualGrid, [tableWidth])

  const params = useParams()

  const { data, loading, error } = useQuery(GET_MODEL_DATA, {
    variables: { id: Number(params.id), model: String(params.model) },
    pollInterval: 5000
  })

  const tableData = data?.getDataForModel?.data || []

  const columns =
    Object.keys(tableData.length > 0 ? tableData[0] : {}).map(
      (item, index) => ({
        title: item,
        dataIndex: Object.keys(tableData[0] || {})[index]
      })
    ) || []

  const widthColumnCount = columns.filter(({ width }) => !width).length

  const mergedColumns = columns.map(column => {
    if (column.width) {
      return column
    }
    return { ...column, width: Math.floor(tableWidth / widthColumnCount) }
  })

  const resetVirtualGrid = () => {
    if (gridRef.current) {
      gridRef.current.resetAfterIndices({
        columnIndex: 0,
        shouldForceUpdate: false
      })
    }
  }
  const chartData = numberOfItemsPerMonth(tableData, dateField)

  const onchange = e => setDateField(e.target.value)

  return (
    <React.Fragment>
      <div className="signin container section">
        <div
          style={{
            cursor: 'pointer',
            marginTop: '40px',
            marginBottom: '20px',
            display: 'inline-block'
          }}
          onClick={() => {
            props.history.goBack()
          }}
        >
          back
        </div>
        {!error && !loading && (
          <h5 className="project-heading">
            <span>{params?.model}</span>{' '}
          </h5>
        )}
        {error && (
          <div className="projects__empty-state">
            <h4 className="title">Model not found for this project.</h4>
            <h6 className="subtitle">
              Maybe you missed that link, but no problem!
            </h6>
          </div>
        )}

        {!error && (
          <React.Fragment>
            <h5>New records count per month:</h5>
            <ResponsiveContainer width="100%" height={500}>
              <BarChart
                width={500}
                height={300}
                maxBarSize={50}
                className="chart"
                data={chartData}
                margin={{
                  top: 5,
                  right: 30,
                  left: 20,
                  bottom: 5
                }}
              >
                <CartesianGrid strokeDasharray="0.1 0.1" />
                <XAxis dataKey="month" stroke="#fff" />
                <YAxis stroke="#fff" />
                <Tooltip cursor={false} />
                <Legend />
                <Bar
                  legendType="star"
                  dataKey="count"
                  fill="#4A61DD"
                  name={`new ${params?.model} count`}
                />
              </BarChart>
            </ResponsiveContainer>
          </React.Fragment>
        )}
        {!error && (
          <div className="mb-24">
            <Input
              type="text"
              label="Change created date field"
              value={dateField}
              onChange={onchange}
              formGroup="desktop"
            >
              <Button color="primary">CHANGE FIELD</Button>
            </Input>
          </div>
        )}
        {!error && (
          <ResizeObserver
            onResize={({ width }) => {
              setTableWidth(width)
            }}
          >
            <Table
              columns={mergedColumns}
              pagination={{ pageSize, total: tableData?.length }}
              loading={loading}
              className="table"
              dataSource={tableData || []}
              scroll={{ x: '100vw' }}
              showSorterTooltip
              onChange={e => setPageSize(e.pageSize || pageSize)}
              rowKey="id"
              components={{
                body: {
                  cell: props => {
                    return (
                      <td {...props}>
                        {props.children[0]}
                        {!['string', 'number', 'boolean'].includes(
                          typeof props.children[1]
                        ) && !isCyclic(props.children[1])
                          ? JSON.stringify(props.children[1])
                          : String(props.children[1])}
                      </td>
                    )
                  }
                }
              }}
            />
          </ResizeObserver>
        )}
      </div>
    </React.Fragment>
  )
}

export default Details
