import React from 'react'
import ReactGA from 'react-ga'
import ReactDOM from 'react-dom'
import { Router } from 'react-router-dom'
import { createBrowserHistory } from 'history'

import App from './App'
import * as serviceWorker from './serviceWorker'

//import './App.css';
import './assets/scss/style.scss'
import 'react-loader-spinner/dist/loader/css/react-spinner-loader.css'

ReactGA.initialize('UA-143062271-3')
ReactGA.pageview(window.location.pathname + window.location.search)

const history = createBrowserHistory()

ReactDOM.render(
  <Router history={history}>
    <App />
  </Router>,
  document.getElementById('root')
)

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister()
