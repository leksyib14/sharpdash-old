import React, { useEffect, useState } from 'react'
import { withRouter, Switch } from 'react-router-dom'
import { ApolloProvider } from '@apollo/react-hooks'
import { ToastContainer, toast } from 'react-toastify'
import UserContext from './contexts/user'
import 'react-toastify/dist/ReactToastify.css'
import client from './graphql/client'
import AppRoute from './utils/AppRoute'
import RequiresAuth from './utils/RequiresAuth'
import ScrollReveal from './utils/ScrollReveal'

// Layouts
import LayoutDefault from './layouts/LayoutDefault'
import LayoutSignin from './layouts/LayoutSignin'

// Views
import Home from './views/Home'
import Signup from './views/Signup'
import Signin from './views/Signin'
import Secondary from './views/Secondary'
import Dashboard from './views/Dashboard'
import Project from './views/Project'
import Details from './views/ModelDetails'
import NodeDocs from './views/Docs/Node'
import NotFound from './views/404'
import PrivacyPolicy from './views/PrivacyPolicy'

global.toast = {
  error: string => toast.error(string.replace('GraphQL error: ', '')),
  success: string => toast.success(string),
  info: string => toast.info(string)
}

const App = props => {
  const [user, setUser] = useState(JSON.parse(localStorage.getItem('user')))

  let scrollReveal

  useEffect(() => {
    setUserContext()
    document.body.classList.add('is-loaded')
    scrollReveal.init()
  }, [props, scrollReveal])

  const setUserContext = () => {
    return setUser(JSON.parse(localStorage.getItem('user')))
  }

  return (
    <ApolloProvider client={client}>
      <UserContext.Provider value={{ user, setUser: setUserContext }}>
        <ScrollReveal
          ref={i => (scrollReveal = i)}
          children={() => (
            <Switch>
              <AppRoute
                exact
                path="/"
                component={Home}
                layout={LayoutDefault}
              />
              <AppRoute
                exact
                path="/Signup"
                component={Signup}
                layout={LayoutDefault}
              />
              <AppRoute
                exact
                path="/Signin"
                component={Signin}
                layout={LayoutDefault}
              />
              <AppRoute
                exact
                path="/secondary"
                component={Secondary}
                layout={LayoutSignin}
              />
              <RequiresAuth
                exact
                path="/dashboard"
                component={Dashboard}
                layout={LayoutDefault}
              />
              <RequiresAuth
                exact
                path="/dashboard/projects/:id"
                component={Project}
                layout={LayoutDefault}
              />
              <RequiresAuth
                exact
                path="/dashboard/projects/:id/model/:model"
                component={Details}
                layout={LayoutDefault}
              />
              <AppRoute
                exact
                path="/docs/node"
                component={NodeDocs}
                layout={LayoutDefault}
              />
              <AppRoute
                exact
                path="/privacy-policy"
                component={PrivacyPolicy}
                layout={LayoutDefault}
              />
              <AppRoute path="*" layout={LayoutDefault} component={NotFound} />
            </Switch>
          )}
        />
        <ToastContainer />
      </UserContext.Provider>
    </ApolloProvider>
  )
}

export default withRouter(props => <App {...props} />)
