clean:
	docker-compose down --remove-orphans && docker-compose -f docker-compose.prod.yml down --remove-orphans
build-dev:
	docker-compose build --no-cache 
dev:
	docker-compose up
build-prod:
	docker-compose -f docker-compose.prod.yml build
prod:
	docker-compose -f docker-compose.prod.yml up
deploy-prod:
	heroku container:push --app sleepy-headland-34702 && heroku container:release --app sleepy-headland-34702
