const axios = require('axios')
const zlib = require('zlib')

const ENDPOINT = 'https://sharpdash.io'

const api = query => {
  return axios({
    url: `${ENDPOINT}/graphql`,
    method: 'post',
    data: {
      query
    }
  })
}

const rawApi = options => {
  return axios({
    url: `${ENDPOINT}/app/sync`,
    method: 'post',
    ...options
  })
}

class SharpDashBoard {
  constructor(apikey) {
    this._apikey = apikey
    this._allIsWell = false
    this._checkIfAllIsWell()
  }

  async _checkIfAllIsWell() {
    try {
      const isAllWell = await api(`
        query {
          isAllWell(apikey: "${String(this._apikey)}")
        }
      `)
      if (isAllWell.data.hasOwnProperty('errors')) {
        console.error('Quickdash: ' + isAllWell.data.errors[0].message)
      } else {
        console.error('Quickdash: connection successful')
        this.allIsWell = true
      }
    } catch (error) {
      console.log(error.response.data)
    }
  }

  async _syncData(modelName, data) {
    try {
      const zipped = zlib.deflateSync(JSON.stringify(data))

      await rawApi({
        data: {
          data: zipped,
          modelName,
          apikey: this._apikey
        },
        headers: {
          'Accept-Encoding': '*'
        }
      })
    } catch (error) {
      console.log(error.response.data)
    }
  }

  async addModelHandler(modelName, func) {
    const data = await func()
    if (data && Array.isArray(data)) {
      await this._syncData(modelName, data)
    } else {
      console.error('Sharpdashboard: Your data should be an iterable/array.')
    }
    setInterval(async () => {
      const data = await func()
      if (data && Array.isArray(data)) {
        await this._syncData(modelName, data)
      } else {
        console.error('Sharpdashboard: Your data should be an iterable/array.')
      }
      // update every 3 mins
    }, 180000)
  }
}

exports.SharpDashBoard = SharpDashBoard
