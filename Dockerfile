# Setup and build the client

FROM node:12.10.0-alpine as client

WORKDIR /usr/app/client/
COPY client/package*.json ./
ENV NODE_ENV production
RUN yarn install
COPY client/ ./
RUN npm run build

# Setup the server

FROM mhart/alpine-node:12

WORKDIR /usr/app/
COPY --from=client /usr/app/client/build/ ./client/build/

RUN apk add redis


WORKDIR /usr/app/server/
COPY server/package*.json ./
RUN npm install
RUN npm i -g concurrently
RUN npm build
COPY server/ ./
RUN npm run build

ENV PORT 8080

ENV NODE_ENV production
ENV CHOKIDAR_USEPOLLING true
ENV DATABASE_URL postgres://opapplzw:12agYQvd5LBvICPi7e5xDVrHuTNvqbyf@drona.db.elephantsql.com:5432/opapplzw
ENV JWT_SECRET oeiunv_owif3-23
ENV PORT 8080

ENV SHARPDASHBOARD_API_KEY f8ccc1ed4795a06418fe5549a91d16e43c0b61d4

EXPOSE 8080

CMD concurrently "/usr/bin/redis-server --appendonly yes" "npm start"
